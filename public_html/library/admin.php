<?php
/* 
This file handles the admin area and functions.
You can use this file to make changes to the
dashboard. Updates to this page are coming soon.
It's turned off by default, but you can call it
via the functions file

*/

/************* ADMIN MENU ORDER *****************/

// useful when styling the backend for a client and you want to rejig the menu
/*
   function custom_menu_order($menu_ord) {
       if (!$menu_ord) return true;
       return array(
        'index.php', // this represents the dashboard link
        'edit.php?post_type=events', // this is a custom post type menu
        'edit.php?post_type=news',
        'edit.php?post_type=articles',
        'edit.php?post_type=faqs',
        'edit.php?post_type=mentors',
        'edit.php?post_type=testimonials',
        'edit.php?post_type=services',
        'edit.php?post_type=page', // this is the default page menu
        'edit.php', // this is the default POST admin menu
    );
   }
   add_filter('custom_menu_order', 'custom_menu_order');
   add_filter('menu_order', 'custom_menu_order');
*/

/************* DASHBOARD WIDGETS *****************/

// disable default dashboard widgets
function disable_default_dashboard_widgets() {
	remove_meta_box('dashboard_right_now', 'dashboard', 'core');       // Right Now Widget
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'core'); // Comments Widget
	remove_meta_box('dashboard_incoming_links', 'dashboard', 'core');  // Incoming Links Widget
	remove_meta_box('dashboard_plugins', 'dashboard', 'core');         // Plugins Widget
	remove_meta_box('dashboard_quick_press', 'dashboard', 'core');     // Quick Press Widget
	remove_meta_box('dashboard_recent_drafts', 'dashboard', 'core');   // Recent Drafts Widget
	remove_meta_box('dashboard_primary', 'dashboard', 'core');         // Wordpress Blog Feed
	remove_meta_box('dashboard_secondary', 'dashboard', 'core');       // Other Wordpress News
	
	// removing plugin dashboard boxes 
	remove_meta_box('yoast_db_widget', 'dashboard', 'normal');         // Yoast's SEO Plugin Widget
	remove_meta_box('rg_forms_dashboard', 'dashboard', 'norma;');      // Gravity Forms Widget
	
}

// Welcome Dashboard Widget
function flutter_welcome_dashboard_widget(){
	echo get_option('blogname');
}

// RSS Dashboard Widget 
function flutter_recent_dashboard_widget() {
	if(function_exists('fetch_feed')) {
		include_once(ABSPATH . WPINC . '/feed.php');               // include the required file
		$feed = fetch_feed('http://www.dessinate.co.uk/feed');     // specify the source feed
		$limit = $feed->get_item_quantity(5);                      // specify number of items
		$items = $feed->get_items(0, $limit);                      // create an array of items
	}
	if ($limit == 0) echo '<div>The RSS Feed is either empty or unavailable.</div>';   // fallback message 
	else foreach ($items as $item) : ?>

	<h4 style="margin-bottom: 0;">
		<a href="<?php echo $item->get_permalink(); ?>" title="<?php echo $item->get_date('j F Y @ g:i a'); ?>" target="_blank">
			<?php echo $item->get_title(); ?>
		</a>
	</h4>
	<p style="margin-top: 0.5em;">
		<?php echo substr($item->get_description(), 0, 200); ?> 
	</p>
	<?php endforeach; 
}

// calling all custom dashboard widgets
function flutter_custom_dashboard_widgets() {
	wp_add_dashboard_widget('flutter_recent_dashboard_widget', 'Recently on Dessinate', 'flutter_recent_dashboard_widget');
	wp_add_dashboard_widget('flutter_welcome_dashboard_widget', 'Welcome', 'flutter_welcome_dashboard_widget');
}


// removing the dashboard widgets
add_action('admin_menu', 'disable_default_dashboard_widgets');
// adding any custom widgets
add_action('wp_dashboard_setup', 'flutter_custom_dashboard_widgets');

/************* CUSTOMIZE ADMIN *******************/

// Custom Backend Footer
function flutter_custom_admin_footer() {
	echo '<span id="footer-thankyou">Developed by <a href="http://www.johnjameskilpatrick.co.uk" target="_blank">John James Kilpatrick</a></span>. Built using <a href="http://www.johnjameskilpatrick.co.uk/flutter/" target="_blank">Flutter</a>.';
}

// adding it to the admin area
add_filter('admin_footer_text', 'flutter_custom_admin_footer');