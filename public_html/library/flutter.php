<?php
/* Welcome to Flutter
This is the core Flutter file where most of the
main functions & features reside. If you have 
any custom functions, it's best to put them
in the functions.php file.

Developed by: John Kilpatrick
URL: http://www.johnjameskilpatrick.co.uk/flutter/
*/

// Wordpress Head Cleanup
function flutter_head_cleanup() {
	// remove header links
	remove_action( 'wp_head', 'feed_links_extra', 3 ); 					  // Category feeds
	remove_action( 'wp_head', 'feed_links', 2 ); 						  // Post and Comment Feed
	remove_action( 'wp_head', 'rsd_link' );                               // EditURI link
	remove_action( 'wp_head', 'wlwmanifest_link' );                       // Windows Live Writer
	remove_action( 'wp_head', 'index_rel_link' );                         // index link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );            // previous link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );             // start link
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 ); // Links for Adjacent Posts
	remove_action( 'wp_head', 'wp_generator' );                           // WP version
}
add_action('init', 'flutter_head_cleanup');
	
// remove WP version
function flutter_version() { 
	return ''; 
}
add_filter('the_generator', 'flutter_rss_version');
	
// loading modernizr, jquery and base css
function flutter_queue_high_js_css() {
  if (!is_admin()) {

		// modernizr & jquery
	    wp_register_script( 'modernizr', get_template_directory_uri() . '/library/js/libs/modernizr.custom.min.js', array(), '1', false );
	    wp_enqueue_script( 'modernizr' );
	    wp_enqueue_script( 'jquery' );

		$conditional_scripts = array(
    		'merriweather' => get_stylesheet_directory_uri().'/library/js/mylibs/merriweather.js',
		);
		wp_localize_script( 'modernizr', 'modernizrScripts', $conditional_scripts );

		//add scripts.js to footer //array of dependencies that must be loaded before this script
	    wp_register_script( 'flutter-js', get_template_directory_uri() . '/library/js/scripts.js', array( 'modernizr', 'jquery' ), '1', true );
	    wp_enqueue_script( 'flutter-js' );

		// register mobile stylesheet
	    wp_register_style( 'flutter-base', get_template_directory_uri() . '/library/css/base.css', array(), '1', 'all' );
	    wp_enqueue_style( 'flutter-base' );
  }
}
	// enqueue base scripts and styles
	add_action('wp_enqueue_scripts', 'flutter_queue_high_js_css', 1);

// loading responsive scripts and styles
function flutter_queue_low_js_css() {
  if (!is_admin()) {
	// responsive stylesheet for those browsers that can read it
    wp_register_style( 'flutter-responsive', get_template_directory_uri() . '/library/css/style.css', array(), '1', '(min-width:481px)' );
    wp_enqueue_style( 'flutter-responsive' );
  }
}
// enqueue responsive scripts and styles
add_action('wp_enqueue_scripts', 'flutter_queue_low_js_css', 999);

	
// WP 3+ Functions & Theme Support
function flutter_theme_support() {
	add_theme_support('post-thumbnails');      // thumbnails
	set_post_thumbnail_size(125, 125, true);   // default thumb size
	add_custom_background();                   // wp custom background
	add_theme_support('automatic-feed-links'); // adds standard feed, comments, cat, tag rss to header
	add_theme_support('post-formats',          // post formats
		array( 
			'aside',   // title less blurb
			'gallery', // gallery of images
			'link',    // quick link to other site
			'image',   // an image
			'quote',   // a quick quote
			'status',  // a Facebook like status update
			'video',   // video 
			'audio',   // audio
			'chat'     // chat transcript 
		)
	);	
	add_theme_support( 'menus' );            // wp menus
	register_nav_menus(                      
		array( 
			'main_nav' => 'The Main Menu',   // main nav in header
			'footer_links' => 'Footer Links' // secondary nav in footer
		)
	);	
}
// launching this stuff after theme setup
add_action('after_setup_theme','flutter_theme_support');	

// adding sidebars to Wordpress (these are created in functions.php)
add_action( 'widgets_init', 'flutter_register_sidebars' );

// custom search form (created in functions.php)
add_filter( 'get_search_form', 'flutter_search' );
	
// extended walker class to customise output of menu
class flutter_walker extends Walker_Nav_Menu {

	function start_el(&$output, $item, $depth, $args) {
		global $wp_query;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';

		$output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '<br /><span class="sub">' . $item->description . '</span>';
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}	

// Header menu
function flutter_main_nav() {
	$walker = new flutter_walker;
    	wp_nav_menu(array( 
    		'menu' => 'main_nav', /* menu name */
    		'theme_location' => 'main_nav', /* where in the theme it's assigned */
    		'container_class' => 'menu clearfix', /* container class */
    		'fallback_cb' => 'flutter_main_nav_fallback', /* menu fallback */
			'walker' => $walker /* customizes the output of the menu */
    	));
}
// Footer menu
function flutter_footer_links() { 
    wp_nav_menu(
    	array(
    		'menu' => 'footer_links', /* menu name */
    		'theme_location' => 'footer_links', /* where in the theme it's assigned */
    		'container_class' => 'footer-links clearfix', /* container class */
    		'fallback_cb' => 'flutter_footer_links_fallback' /* menu fallback */
    	)
	);
}
 
// Fallback for header menu
function flutter_main_nav_fallback() { 
	wp_page_menu( 'show_home=Home&menu_class=menu' ); 
}

// Fallback for footer menu
function flutter_footer_links_fallback() { 
	/* */ 
}
	
// Related Posts (call using flutter_related_posts(); )
function flutter_related_posts() {
	echo '<ul id="flutter-related-posts">';
	global $post;
	$tags = wp_get_post_tags($post->ID);
	if($tags) {
		foreach($tags as $tag) { $tag_arr .= $tag->slug . ','; }
        $args = array(
        	'tag' => $tag_arr,
        	'numberposts' => 5,
        	'post__not_in' => array($post->ID)
     	);
        $related_posts = get_posts($args);
        if($related_posts) {
        	foreach ($related_posts as $post) : setup_postdata($post); ?>
	           	<li class="related_post"><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
	        <?php endforeach; } 
	    else { ?>
            <li class="no_related_post">No Related Posts Yet!</li>
		<?php }
	}
	wp_reset_query();
	echo '</ul>';
}

?>
