<!doctype html>  
<!--[if IEMobile 7 ]> <html <?php language_attributes(); ?> class="no-js iem7"> <![endif]-->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gte IE 8)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!-->
<html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	
	<title><?php wp_title(''); ?></title>

	<meta name="description" content="">
	<meta name="author" content="John Kilpatrick">
	<meta name="title" content="<?php wp_title(''); ?>" >
	<meta name="geo.region" content="GB" />
	<meta name="geo.placename" content="London" />
	<meta name="geo.position" content="51.553386;-0.074427" />
	<meta name="ICBM" content="51.553386, -0.074427" />
	
	<!-- mobile optimized -->
	<meta name="viewport" content="width=device-width">
	
	<!-- allow pinned sites -->
	<meta name="application-name" content="<?php bloginfo('name'); ?>" />
	
	<!-- icons & favicons (for more: http://themble.com/support/adding-icons-favicons/) -->
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	
	<!-- wordpress head functions -->
	<?php wp_head(); ?>

	<!-- end of wordpress head -->
	
	<!-- load all styles for IE -->
	<!--[if (lt IE 9) & (!IEMobile)]>
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/library/css/ie.css">	
	<![endif]-->
	
</head>

<body <?php body_class(); ?>>

	<div id="container">

		<nav role="navigation" class="nav" style="display: block; ">
		    <div class="container_24">
		      <div class="nav-item" data-target="#top">Link 1</div>
		      <div class="nav-item" data-target="#middle">Link 2</div>
		      <div class="nav-item" data-target="#bottom">Link 3</div>
		    </div>
		  </nav>

		<header role="header" class="header">
			<div class="inner-header">
				<article>
					<p>My money's in that office, right? If she start giving me some bullshit about it ain't there, and we got to go someplace else and get it, I'm gonna shoot you in the head then and there. </p>
				</article>
				<aside class="right">
					<p>The path of the righteous man is beset on all sides by </br> the iniquities of the selfish and the tyranny of evil men.</p>
				</aside>
			</div> 
			<div class="ribbon bottom"></div>
		</header>
