Flutter Wordpress 3+ Framework/Starter Theme
=================

# Description #

Just like giving SMA to a kid, give your wordpress project a good start in life.

A responsive, mobile-first framework/starter theme built on 1140 grid
and HTML5 Boilerplate

## About ##

All SCSS files are in the /compass folder outside of the public_html.

## Command Line Commands ##

To compile the projects SCSS files 

compass compile [path/to/project]

Watch the project for changes

compass watch [path/to/project]

To compile the projects HAML files

haml index.haml index.html

## Documentation for Libraries ##

* [Compass](http://compass-style.org/) 
* [SASS](http://sass-lang.com/) 
* [HAML](http://haml.info/) 